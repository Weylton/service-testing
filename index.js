function testService(events) {
  const visitors = new Set();

  for (let event of events) {
    if (event[1] === 'in' && !visitors.has(event[0])) {
      visitors.add(event[0]);
    } else if (event[1] === 'out' && visitors.has(event[0])) {
      visitors.delete(event[0]);
    } else {
      return false;
    }
  }

  return visitors.size === 0;
}

module.exports = testService;
